
<div class="container" style="height: 550px; width: 550px; margin: 15px auto; border: 1px solid #5b9bd5; display: flex; justify-content:center; flex-direction: column; align-items: center;">
<div style="width: 450px; text-align: left;">
<label style="color: red">
<?php foreach($error as $value){
    echo "$value <br>"; 
}
?>
</label>
</div>

<form method="post" enctype="multipart/form-data" action="">
<!-- name -->
 <div style="display: flex; width: 450px; justify-content: space-around; margin: 10px auto"> 
 <div style="width: 110px; background-color: #5b9bd5; text-align: center; padding: 10px 0; border: 1px solid #42719b;"> 
 <label style="color: white" >  Họ và tên<span style = "color:red"> * </span>  </label> 

</div>  
<input type="text" name="name" style="margin-left: 20px; height: 40px; width: 320px; border: 1px solid #42719b;"> 
</div> 
 <!-- gender -->
<div style="display: flex; width: 450px; justify-content: left; margin: 10px auto"> 
<div style="width: 109px; background-color: #5b9bd5; text-align: center; padding: 10px 0; border: 1px solid #42719b;"> 
<label style="color: white;">  Giới tính<span style = "color:red"> * </span>  </label> 
</div> 

<div style="display:flex; align-items: center"> 

<?php
$counter = 1;
for ($i = 0; $i < count($gender); $i++) {
    $temp = $i + $counter;
    ?>
    <input type='radio' id=<?php echo $gender[$i];?> name='gender' style='margin-left:30px;' value=<?php echo $gender[$i];?> >
    <label for=<?php echo $gender[$i];?>><?php echo $gender[$i];?></label>
    <?php
    $counter = -1;
}
    ?>

</div>
</div>

 <!-- Select faculty -->
<div  style="display: flex; width: 450px; margin: 10px auto">
<div style="width: 110px; background-color: #5b9bd5; text-align: center; padding: 10px 0; border: 1px solid #42719b;">
<label style="color: white"> Phân khoa<span style = "color:red"> * </span> </label>
</div>	
<select name="faculty" id="faculty" style="margin-left: 20px; height: 40px; width: 170px; border: 1px solid #42719b;">
<?php 
foreach ($faculties as $faculty) {
    echo "<option value=$faculty[0]>$faculty[1]</option>";
}
?>
</select>
<!-- Date of birth -->
</div>
<div  style="display: flex; width: 450px; margin: 10px auto">
<div style="width: 110px; background-color: #5b9bd5; text-align: center; padding: 10px 0; border: 1px solid #42719b;">
<label style="color: white" >Ngày sinh<span style = "color:red"> * </span> </label>
</div>
<input type="text" id="txtDate" name="birth" style="margin-left: 20px; height: 40px; width: 165px; border: 1px solid #42719b;" placeholder="dd/mm/yyyy">
</div>



<!-- address -->
<div  style="display: flex; width: 450px; margin: 10px auto">
<div style="width: 110px; background-color: #5b9bd5; text-align: center; padding: 10px 0; border: 1px solid #42719b;">
<label style="color: white" > Địa chỉ  </label>
</div>
<input type="text" name="address" style="margin-left: 20px; height: 40px; width: 320px; border: 1px solid #42719b;">
</div>

 <!-- Upload file image -->
<div  style="display: flex; width: 450px; margin: 10px auto">
<div style="width: 110px; background-color: #5b9bd5; text-align: center; padding: 10px 0; border: 1px solid #42719b;">
<label style="color: white" > Hình ảnh  </label>
</div>
<input type="file" name="imageFile" id="imageFile" style="margin-left: 20px; height: 40px; width: 320px;">
</div>

<!-- BTN Submit -->

<div style="width: 300px; margin: 0 auto; display: flex; justify-content: center;">
<input type="submit" value="Đăng ký" style="height: 45px; width: 130px; font-size: 15px; background-color: #5b9bd5; border-radius: 5px; color: white;">
</div>
</form>
</div>