<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><Day04></Day04></title>
	<style>
		@import url('https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400&display=swap');
        img {
            
            height: 100px;
        }
        label{ 
            font-weight: 400;
        }
	</style>
</head>
<body style="height: 100vh; display: flex;  justify-content:center; align-items: center;">
    <div class="container" style="height: 550px; width: 550px; margin: 0 auto; border: 1px solid #5b9bd5;display: flex; justify-content:center; flex-direction: column; align-items: center;">
        <div style="display: flex; width: 450px; justify-content: space-around; margin: 10px auto">
            <div style="width: 110px; background-color: #5b9bd5; text-align: center; padding: 10px 0; border: 1px solid #42719b;">
                <label style="color: white" ><?php echo "Họ và tên"?></label>
            </div>
            <label style="margin-left: 20px; height: 40px; width: 320px; display: flex; align-items: center; font-size: 16px"><?php echo $_SESSION["name"]; ?></label>
        </div>

        <div style="display: flex; width: 450px; justify-content: space-around; margin: 10px auto">
            <div style="width: 110px; background-color: #5b9bd5; text-align: center; padding: 10px 0; border: 1px solid #42719b;">
                <label style="color: white" ><?php echo "Giới tính"?></label>
            </div>
            <label style="margin-left: 20px; height: 40px; width: 320px; display: flex; align-items: center; font-size: 16px"><?php 
            if (isset($_SESSION["gender"])){
                echo $_SESSION["gender"];
            }
            ?></label>
        </div>

        <div style="display: flex; width: 450px; justify-content: space-around; margin: 10px auto">
            <div style="width: 110px; background-color: #5b9bd5; text-align: center; padding: 10px 0; border: 1px solid #42719b;">
                <label style="color: white" ><?php echo "Phân khoa"?></label>
            </div>
            <label style="margin-left: 20px; height: 40px; width: 320px; display: flex; align-items: center; font-size: 16px">
            <?php 
            foreach ($faculties as $faculty) {
                if ($_SESSION["faculty"] == $faculty[0]) {
                  echo $faculty[1];
                }
              }
            ?>
            </label>
        </div>
        <!-- Ngày sinh -->
        <div style="display: flex; width: 450px; justify-content: space-around; margin: 10px auto">
            <div style="width: 110px; background-color: #5b9bd5; text-align: center; padding: 10px 0; border: 1px solid #42719b;">
                <label style="color: white" ><?php echo "Ngày sinh"?></label>
            </div>
            <label style="margin-left: 20px; height: 40px; width: 320px; display: flex; align-items: center; font-size: 16px">
            <?php 
             
                echo  $_SESSION["birth"]; 
            
            ?>
            </label>
        </div>

        <!-- Địa chỉ -->
        <div style="display: flex; width: 450px; justify-content: space-around; margin: 10px auto">
            <div style="width: 110px; background-color: #5b9bd5; text-align: center; padding: 10px 0; border: 1px solid #42719b;">
                <label style="color: white" ><?php echo "Địa chỉ"?></label>
            </div>
            <label style="margin-left: 20px; height: 40px; width: 320px; display: flex; align-items: center; font-size: 16px">
            <?php 
            
            if (!empty($_SESSION["address"])){
                echo  $_SESSION["address"]; 
            }
            ?>
            </label>
        </div>
            <!-- Image file -->
            <div style="display: flex; width: 450px; justify-content: space-around; margin: 10px auto">
            <div style="width: 110px; background-color: #5b9bd5; text-align: center; padding: 10px 0; border: 1px solid #42719b;">
                <label style="color: white" ><?php echo "Hình ảnh"?></label>
            </div>
            <label style="margin-left: 20px; height: 40px; width: 320px; display: flex; align-items: center; font-size: 16px">
            <?php 
            if (!empty($_SESSION["image"])){
                ?>
                <img style ="display: block; margin-top: auto;margin-right: auto; " src="<?php echo $_SESSION["image"] ?>" alt="">
            <?php
            }
            ?>
            </label>
        </div>
        <!-- BTN Submit -->
        <form method="post" enctype="multipart/form-data" action="">
        <div style="width: 300px; margin: 85px 20px auto; display: flex; justify-content: center;" >
        <button type="submit" value="Xác nhận" name="submit_regist" style="height: 45px; width: 130px; font-size: 15px; background-color: #5b9bd5; border-radius: 5px; color: white;">
            Xác nhận
        </button>
        </div>
        </form>
    </div>
</body>
</html>