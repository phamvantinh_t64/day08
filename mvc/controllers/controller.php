<?php 
	class Controller extends Model
	{
		private $model;
		function __construct()
		{
			$this->model = new Model(); 
		}
		public function Controllers()
		{
			$gender = array("Nữ", "Nam");
			$faculties = array(array("", ""), array("MAT", "Khoa học máy tính"), array("KDL", "Khoa học dữ liệu"));

			if (isset($_GET['page'])) {
                $page = $_GET['page'];
            }else {
                $page = "search";
            }

            switch ($page) {
            	case 'search':
					if (isset($_GET['method'])) {
		                $method = $_GET['method'];
						if (isset($_GET['id'])) {
							$id = $_GET['id']; 
						}
						
		            }
					$students = array(
										"Trần Văn A"=>"MAT",
										"Nguyễn Thị B"=>"KDL",
										"Hoàng Văn C"=>"MAT",
										"Đinh Thị D"=>"KDL",
								);
					if ($_SERVER["REQUEST_METHOD"] == "POST"){
						$_COOKIE["faculty"] = $_POST["faculty"];
						$_COOKIE["keyword"] = $_POST["keyword"];
						
					}
            		include_once "./mvc/views/".$page.".php";
					break;
				case 'regist':
					
					$reg = "/^[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}$/";
					$error = array();
					$valid = true;
					$checkupload = true;
					if ($_SERVER["REQUEST_METHOD"] == "POST") {

						if (empty($_POST["name"])) {
							array_push($error,"Hãy nhập tên.");
							$valid = false;
						} 
						if (empty($_POST["gender"])) {
							array_push($error,"Hãy chọn giới tính.");
							$valid = false;
						} 
						if ($_POST["faculty"] == "None") {
							array_push($error,"Hãy chọn phân khoa.");
							$valid = false;
						}
						if (empty($_POST["birth"])) {
							array_push($error,"Hãy nhập ngày sinh.");
							$valid = false;
						} elseif (!preg_match($reg,$_POST["birth"])) {
							array_push($error,"Hãy nhập ngày sinh đúng định dạng.");
							$valid = false;
						}    
						if (!isset($_FILES["imageFile"]))
						{
							array_push($error,"File upload đã tồn tại.");
							$valid = false;
						}
						if ($_FILES["imageFile"]['error'] != 0)
						{
							$vaAid = false;
						}
						$target_dir    = "public/upload/";
						$target_file   = $target_dir . basename($_FILES["imageFile"]["name"]);
						$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
						$checktypes    = array('jpg', 'png', 'jpeg', 'gif');
						$maxfilesize   = 800000;


					if(isset($_POST["submit"])) {
						$check = getimagesize($_FILES["imageFile"]["tmp_name"]);
						if($check !== false)
						{
							$valid = true;
						}
						else
						{
							$valid = false;
						}
					}
						if (file_exists($target_file))
						{
							array_push($error,"File upload không tồn tại.");
							
							$valid = false;
						} elseif (!in_array($imageFileType,$checktypes))
						{
							array_push($error,"File upload phải là định dạng JPG, PNG, JPEG, GIF");
							$valid = false;
						}
						if ($_FILES["imageFile"]["size"] > $maxfilesize)
						{
							array_push($error, "Không được upload ảnh lớn hơn $maxfilesize (bytes).");
							$valid = false;
						}
						if ($valid) {
							$_SESSION["name"] = $_POST["name"];
							$_SESSION["gender"] = $_POST["gender"];
							$_SESSION["faculty"] = $_POST["faculty"];
							$_SESSION["birth"] = $_POST["birth"];
							$_SESSION["address"] = $_POST["address"];
							if (!file_exists($target_dir)){
								mkdir($target_dir, 0777, true);
							}
							$target_file = $target_dir . pathinfo($_FILES['imageFile']['name'], PATHINFO_FILENAME)."_".date('YmdHis').".".pathinfo($target_file,PATHINFO_EXTENSION);
							if (move_uploaded_file($_FILES["imageFile"]["tmp_name"], $target_file)) {
								$_SESSION["image"] =$target_file;
							} else {
								$_SESSION["image"] = "Sorry, there was an error uploading your file.";
							}
							
							echo "<script>location.href = 'index.php?page=do_regist';</script>";
							exit();
						}  
					}
						include_once "./mvc/views/".$page.".php";
						break;
				case 'do_regist':
					
					include_once "./mvc/views/".$page.".php";
					break;
				
				}
		}
	}

?>